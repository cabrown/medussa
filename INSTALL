= Medussa Build & Installation Instructions =

== On Linux ==

=== Build Requirements ===

You will need the devel packages for python and numpy (>= 1.3). Header files for portaudio and libsndfile are included. 

=== Runtime requirements ===

  * [http://www.python.org Python]. Any version >= 2.6. 
  * [http://numpy.scipy.org/ NumPy]. Version 1.3 or later should work.
  * [http://www.portaudio.com Portaudio]. Only tested with v19, November 21, 2011, SVN rev 1788 (later versions should be fine).
  * [http://www.mega-nerd.com/libsndfile/ Libsndfile]. Only tested with version 1.0.25, July 13 2011 (later versions should be fine).


=== Building and installing ===

(This is assuming that you have a compatible Python and NumPy installed)

From a command prompt, go the the Medussa directory and type:
 
 {{{
 reinstall_linux.sh 2.7
 }}}
 
(Replace "2.7" with whatever compatible Python version you want to use.)

This removes any existing version, rebuilds the lib, and installs the package.


=== Building the Medussa shared lib ===

(this doesn't actually install anything, it only builds the lib)

From a command prompt, type:

 {{{
 cd lib/build/linux
 ./build.sh 2.7
 # lib will be at py2.7/libmedussa.so
 cd ../../..
 }}}

----

== On Windows ==
 
=== Building and installing ===

This Assumes that you have a compatible Python and NumPy (see above) installed, and you're using MSVC 10. You don't need libsndfile or Portaudio installed, as thosed dlls are included with Medussa.
 
From a command prompt, go the the Medussa directory and type:
 
 {{{
 reinstall_win.bat 2.7
 }}}

(Replace "2.7" with whatever Python version you want to use.)
 
This uninstalls a previous version, rebuilds the dll, repackages the msi, & installs.


=== Building the Medussa DLL ===

(this doesn't actually install anything, it only builds the lib):
	
  # Open the Visual Studio 10 project at: \medussa\lib\build\win\msvc10\medusa.vcxproj
  # Select the Solution Configuration according to the Python version you're using (eg., Release Py2.7).
  # Choose Build Solution from the Build menu (or hit F7).

